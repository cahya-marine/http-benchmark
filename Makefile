http-1:
	h2load -c${conn} -m${multi} -n${req} --h1 ${url}

http-2:
	h2load -c${conn} -m${multi} -n${req} ${url}

http-1-post:
	h2load  -c${conn} -m${multi} -n${req} ${url} -d testpayload.json --h1 --header 'Content-Type: application/json'

http-2-post:
	h2load  -c${conn} -m${multi} -n${req} ${url} -d testpayload.json --header 'Content-Type: application/json'