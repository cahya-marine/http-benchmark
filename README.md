# http-benchmark



## HTTP/1 vs HTTP/2 

This repo contains 2 feature to test , using `nghttp2 (h2load)` and using `net/http`

## Download go pkg
```bash
go mod tidy
```

## Install nghttp2 on mac osx
```bash
brew install nghttp2
```

## Install nghttp2 on linux
```bash
sudo apt-get -y install nghttp2
```

## Running benchmark using `net/http` golang
```
c : total concurrent connection
http : http protocol that used (1 or 2)
url : test url 
```
```bash
go run *.go -c 100 -http 1 -url https://changewithrealtesturl.com
```

## Running benchmark using `h2load` golang
```
conn : total tcp open connection
multi : max concurrent streams to issue per session. When http/1.1 is used, this specifies the number of HTTP pipelining requests in-flight.
req : number of requests across all clients.
url : test url
```

```bash
make conn=100 multi=100 req=100 url=https://changewithrealtesturl.com http-1
```





